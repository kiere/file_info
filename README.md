# FileInfo

This library aims to provide information about a file, based on its contents.

## Dependency

```
{:file_info, "~> 0.0.5"}
```